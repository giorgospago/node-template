const socketio  = require('socket.io');

require('dotenv').config();

const app = require('./app');

// Initialize Sockets && app listen
global.io = socketio(app.listen(process.env.PORT || 3000));
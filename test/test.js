const request = require('supertest');

process.env.MONGO_URI = "mongodb://superuser:superpassword@develobird.gr:37017/MochaTest?authSource=admin";

const app = require('../app');
const agent = request.agent(app);

describe("API", function(){

    it("Api home page works", function(done){
        agent
            .get('/api')
            .expect(200)
            .expect('api page')
            .end(done);
    });
});

let carIds = [];

describe("Cars", function(){
    
    it("Truncate cars", function(done){
        Car.collection.remove();
        done();
    });

    it("create new car", function(done){
        agent
            .post('/api/cars')
            .send({
                brand: 'Ferrari',
                model: 'Enzo'
            })
            .expect(201)
            .expect('Content-Type', /json/)
            .expect({message: "car created"})
            .end(done);
    });

    it("get car list", function(done){
        agent
            .get('/api/cars')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(function(err, res){
                carIds = res.body.map(item => item._id);
                done();
            });
    });
});

describe("Customers", function(){
    
    it("Truncate Customers", function(done){
        Customer.collection.remove();
        done();
    });

    it("Create customer", function(done){
        agent
            .post('/api/customers')
            .send({
                name: 'Karydakis',
                cars: carIds
            })
            .expect(201)
            .expect('Content-Type', /json/)
            .end(done);
    });

    it("Get list of customers", function(done){
        agent
            .get('/api/customers')
            .expect(200)
            .expect('Content-Type', /json/)
            .end(done);
    });

});